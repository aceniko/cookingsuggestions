function loadRecipesForCategory_InitialLoad(e, id) {
	var listItem, dataList = e.getElementById('dataList');
	//var items = [];
	//console.log(dataList.categoryid);
	$.ajax({
		type:'GET',
		url:'http://89.205.7.204/cs/Json/GetRecipesForCategory',
		data:{
			id:id
		}
		,
		dataType: 'json',
		success: function(data){
			//console.log(data);
			var json = $.parseJSON(data);
			$.each(json, function(index, element) {
				//console.log(element.Title);
				listItem = document.createElement('div');
				listItem.setAttribute('data-bb-type','item');
				listItem.setAttribute('data-bb-img','http://89.205.7.204/cs/images/'+element.ImageUrl);
				listItem.setAttribute('data-bb-title',element.Title);
				listItem.setAttribute('onclick','bb.pushScreen(\'recipe.htm\', \'recipe\', { \'recipeid\' :'+element.ID+'});');
				dataList.appendItem(listItem);
				//items.push(listItem)
			});
		},
		error: function(data){
		
		}
		});
		}
		//document.getElementById('dataList').refresh(items);
		//datalist.refresh(items);
