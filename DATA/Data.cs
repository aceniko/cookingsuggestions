﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq;
namespace DATA
{
    public class Data : IData, IDisposable
    {
        private dbCookingDataContext db;

        public Data()
        {
            db = new dbCookingDataContext();
        }
        public Data(dbCookingDataContext _db) { this.db = _db; }

        public void Dispose()
        {
            this.db.Dispose();
        }


        #region Category
        public bool InsertOrUpdateCategory(int? id, string userName, string name, string displayName, string imageUrl, bool isActive, out string Error)
        {
            int? result = 0;
            string errorMsg = "";
            db.InsertOrUpdateCategory(id, userName, name, displayName, imageUrl, isActive, ref result, ref errorMsg);
            Error = errorMsg;
            if (result > 0)
                return true;
            return false;
        }


        public IList<GetCategoryListResult> getCategories()
        {
            return db.GetCategoryList().ToList();
        }

        //public IList<GetCategoriesForRecipeResult> getCategoriesForRecipeID(int id)
        //{
        //    return db.GetCategoriesForRecipe(id).ToList();
        //}

        public System.Data.Linq.ISingleResult<GetCategoryByIDResult> getCategoryByID(int id)
        {
            return db.GetCategoryByID(id);
        }

        public IList<GetActiveCategoriesResult> getActiveCategories()
        {
            return db.GetActiveCategories().ToList();
        }
        #endregion

        #region Recipe
        public bool InsertOrUpdateRecipe(int? id, string Username, string Title, string Content, bool isActive, string imageUrl, string hashID,string day, string holidayTag, out string errorMsg)
        {
            int? result = 0;
            string error = "";
            db.InsertOrUpdaterRecipe(id, Username, Title, Content, imageUrl, isActive, hashID, day, holidayTag, ref result, ref error);
            errorMsg = error;
            if (result > 0)
                return true;
            return false;
        }

        public IList<GetRecipeForCategoryResult> getRecipesForCategoryID(int id)
        {
            return db.GetRecipeForCategory(id).ToList();
        }

        public IList<GetActiveRecipesByCategoryIDResult> getActiveRecipesByCategoryID(int id)
        {
            return db.GetActiveRecipesByCategoryID(id).ToList();
        }

        public System.Data.Linq.ISingleResult<GetRecipeByIDResult> getRecipeByID(int id)
        {
            return db.GetRecipeByID(id);
        }

        public ISingleResult<getTodayRecipeResult> getTodayRecipe(string today) {
            return db.getTodayRecipe(today);
        }
        #endregion

        #region Category_Recipe
        public bool insertOrUpdateCategory_Recipe(int? id, int recipeID, int categoryID, out string errorMsg)
        {
            int? result = 0;
            string error = "";
            db.InsertOrUpdateCategory_Recipe(id, categoryID, recipeID, ref result, ref error);
            errorMsg = error;
            if (result > 0)
                return true;
            return false;
        }
        #endregion

        #region User
        public IList<GetUserListResult> getUsers()
        {
            return db.GetUserList().ToList();
        }

        public System.Data.Linq.ISingleResult<GetUserByIDResult> getUserByID(int id)
        {
            return db.GetUserByID(id);
        }
        public bool insertOrUpdateUser(int? id, string username, string firstName, string lastName, out string errorMsg) {
            int? result = 0;
            string err = string.Empty;
            db.InsertOrUpdateUser(id, username, firstName, lastName, ref result, ref err);
            errorMsg = err;
            return result > 0 ? true : false;
        }
        #endregion

        #region Menu
        public bool InsertOrUpdateMenu(int? id, string name, string imageUrl, bool isActive, short sequence, out string errorMsg)
        {
            int? result = 0;
            string error = "";
            db.InsertOrUpdateMenu(id, name, imageUrl, isActive, sequence, ref result, ref error);
            errorMsg = error;
            if (result > 0)
                return true;
            return false;
        }

        public IList<GetMenuListResult> getMenuList()
        {
            return db.GetMenuList().ToList();
        }

        public IList<GetActiveMenuListResult> getActiveMenuList()
        {
            return db.GetActiveMenuList().ToList();
        }

        public System.Data.Linq.ISingleResult<GetMenuByIDResult> getMenuByID(int id)
        {
            return db.GetMenuByID(id);
        }
        #endregion

        #region Video
        public bool InsertOrUpdateVideo(int? id, string username, string title, string description, string url, bool isActive, out string errorMsg)
        {
            int? result = 0;
            string error = "";
            db.InsertOrUpdateVideo(id, username, title, description, url, isActive, ref result, ref error);
            errorMsg = error;
            if (result > 0)
                return true;
            return false;
        }

        public IList<GetVideosListResult> getVideos()
        {
            return db.GetVideosList().ToList();
        }

        public IList<GetActiveVideoListResult> getActiveVideos()
        {
            return db.GetActiveVideoList().ToList();
        }

        public System.Data.Linq.ISingleResult<GetVideoByIDResult> getVideoByID(int id)
        {
            return db.GetVideoByID(id);
        }
        #endregion

        #region favorite
        public bool insertFavorite(long uuid, int recipeID, out string error) {
            int? result = 0;
            string err = string.Empty;
            db.InsertFavorite(uuid, recipeID, ref result, ref err);
            error = err;
            return result > 0 ? true : false;
        }
        public IList<GetFavoritesResult> getFavorites(long uuid) {
            return db.GetFavorites(uuid).ToList();
        }
        #endregion

        #region recoomended
        public bool insertRecommended(int recipeID, out string error) {
            int? result = 0;
            string err = string.Empty;
            db.InsertRecommended(recipeID, ref result, ref err);
            error = err;
            return result > 0 ? true : false;
        }
        public IList<GetRecommendedResult> getRecommended() {
            return db.GetRecommended().ToList();
        }
        #endregion

        #region Vegeterian
        public IList<GetVegeterianRecipesResult> getVegeterianRecipes() {
            return db.GetVegeterianRecipes().ToList();
        }
        #endregion

        #region Holiday
        public IList<GetRecipesByHolidayTagResult> getRecipesByHolidayTag(string tag) {
            return db.GetRecipesByHolidayTag(tag).ToList();
        }
        #endregion
    }
}
