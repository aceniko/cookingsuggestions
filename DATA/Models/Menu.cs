﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DATA.Models
{
    public class Menu
    {
        public int? ID { get; set; }
        [Required]
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public short Sequence { get; set; }
        public bool IsActive { get; set; }
    }
}
