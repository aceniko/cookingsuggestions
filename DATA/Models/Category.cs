﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DATA.Models
{
    public class Category
    {
        public int? ID { get; set; }
        public string UserName { get; set; }
        [Required(ErrorMessage="poleto e zadolzitelno")]
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public bool IsActive { get; set; }
        public string ImageUrl { get; set; }
    }
}
