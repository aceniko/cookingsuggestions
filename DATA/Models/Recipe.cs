﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DATA.Models
{
    public class Recipe
    {
        public int? ID { get; set; }
        public string Username { get; set; }
        [Required]
        public string Title { get; set; }
        public string Content { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string ImageUrl { get; set; }
        public string HashID { get; set; }
        public string Day { get; set; }
        public string HolidayTag { get; set; }
        public virtual int categoryID { get; set; }
    }
}
