﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DATA
{
    public interface IData
    {
        #region Category

        bool InsertOrUpdateCategory(int? id, string userName, string name, string displayName, string imageUrl, bool isActive, out string Error);
        IList<GetCategoryListResult> getCategories();
        //IList<GetCategoriesForRecipeResult> getCategoriesForRecipeID(int id);
        ISingleResult<GetCategoryByIDResult> getCategoryByID(int id);
        IList<GetActiveCategoriesResult> getActiveCategories();

        #endregion

        #region Recipe

        bool InsertOrUpdateRecipe(int? id, string Username, string Title, string Content, bool isActive, string imageUrl, string hashID, string day, string holidayTag, out string errorMsg);
        IList<GetRecipeForCategoryResult> getRecipesForCategoryID(int id);
        IList<GetActiveRecipesByCategoryIDResult> getActiveRecipesByCategoryID(int id);
        ISingleResult<GetRecipeByIDResult> getRecipeByID(int id);
        ISingleResult<getTodayRecipeResult> getTodayRecipe(string today);
        #endregion

        #region Category_Recipe

        bool insertOrUpdateCategory_Recipe(int? id, int recipeID, int categoryID, out string errorMsg);


        #endregion

        #region User

        IList<GetUserListResult> getUsers();
        ISingleResult<GetUserByIDResult> getUserByID(int id);
        bool insertOrUpdateUser(int? id, string username, string firstName, string lastName, out string errorMsg);
        #endregion

        #region Menu

        bool InsertOrUpdateMenu(int? id, string name, string imageUrl, bool isActive, short sequence, out string errorMsg); 
        IList<GetMenuListResult> getMenuList();
        IList<GetActiveMenuListResult> getActiveMenuList();
        ISingleResult<GetMenuByIDResult> getMenuByID(int id);

        #endregion

        #region Video

        bool InsertOrUpdateVideo(int? id, string username, string title, string description, string url, bool isActive, out string errorMsg);
        IList<GetVideosListResult> getVideos();
        IList<GetActiveVideoListResult> getActiveVideos();
        ISingleResult<GetVideoByIDResult> getVideoByID(int id);

        #endregion

        #region favorite
        bool insertFavorite(long uuid, int recipeID, out string error);
        IList<GetFavoritesResult> getFavorites(long uuid);
        #endregion

        #region recommended
        bool insertRecommended(int recipeID, out string error);
        IList<GetRecommendedResult> getRecommended();
        #endregion

        #region Vegeterian
        IList<GetVegeterianRecipesResult> getVegeterianRecipes();
        #endregion

        #region Holiday
        IList<GetRecipesByHolidayTagResult> getRecipesByHolidayTag(string tag);
        #endregion
    }
}
