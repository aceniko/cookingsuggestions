﻿using DATA;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CookingSuggestions.Controllers
{
    public class JsonController : Controller
    {
        //
        // GET: /Json/
        private readonly IData _repository;

        public JsonController() : this(new Data()) { }
        public JsonController(IData data) { _repository = data; }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetTodayRecipe() {
            DateTime t = DateTime.UtcNow;
            DateTimeFormatInfo dateTimeFormat = new CultureInfo("en-US").DateTimeFormat;
            string today = t.ToString("dddd", dateTimeFormat);
            var recipe = _repository.getTodayRecipe(today);
            string json = JsonConvert.SerializeObject(recipe);
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetMainMenu() {
            var menu = _repository.getActiveMenuList();
            string json = JsonConvert.SerializeObject(menu);

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCategories() {
            var categories = _repository.getActiveCategories();

            string json = JsonConvert.SerializeObject(categories);
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult GetRecipesForCategory(int id) {

            var recipes = _repository.getActiveRecipesByCategoryID(id);
            string json = JsonConvert.SerializeObject(recipes);
            return Json(json, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetVideos() {

            var videos = _repository.getActiveVideos();
            string json = JsonConvert.SerializeObject(videos);
            return Json(json, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetVideoByID(int id) {
            var video = _repository.getVideoByID(id);
            string json = JsonConvert.SerializeObject(video);
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetRecipeByID(int id) {
            var recipe = _repository.getRecipeByID(id);
            string json = JsonConvert.SerializeObject(recipe);
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetVegeterian() {
            var vegeterian = _repository.getVegeterianRecipes();
            string json = JsonConvert.SerializeObject(vegeterian);
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetRecommended() {
            var recipes = _repository.getRecommended();
            string json = JsonConvert.SerializeObject(recipes);
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetFavorites(long uuid) {
            var recipes = _repository.getFavorites(uuid);
            string json = JsonConvert.SerializeObject(recipes);
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetRecipesByHoliday(string holidayTag) {
            var recipes = _repository.getRecipesByHolidayTag(holidayTag);
            string json = JsonConvert.SerializeObject(recipes);
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddFavorite(int recipeID, long uuid) {
            string error = string.Empty;
            if (_repository.insertFavorite(uuid, recipeID, out error))
                return Json(new { response = "successful" }, JsonRequestBehavior.AllowGet);
            return Json(new { response = error }, JsonRequestBehavior.AllowGet);
        }
    }
}
