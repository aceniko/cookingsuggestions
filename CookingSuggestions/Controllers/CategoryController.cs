﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DATA;
namespace CookingSuggestions.Controllers
{
    [Authorize]
    public class CategoryController : Controller
    {
        private readonly IData _repository;

        public CategoryController() : this(new Data()) { }
        public CategoryController(IData data) { _repository = data; }

        public ActionResult Index()
        {
            IList<GetCategoryListResult> model = _repository.getCategories();
            
            return View(model);
        }

        public ActionResult Create()
        {
            DATA.Models.Category model = new DATA.Models.Category();
            model.UserName = User.Identity.Name;
            return View(model);
        }

        //
        // POST: /Category/Create

        [HttpPost]
        public ActionResult Create(DATA.Models.Category model)
        {
            string error = "";
            if (_repository.InsertOrUpdateCategory(null, model.UserName, model.Name, model.DisplayName, model.ImageUrl, model.IsActive, out error))
                return RedirectToAction("Index");
            ModelState.AddModelError("", error);
            return View(model);
        }

        //
        // GET: /Category/Edit/5

        public ActionResult Edit(int id)
        {
            DATA.Models.Category model = (from n in _repository.getCategoryByID(id) select new DATA.Models.Category { 
                ID = n.ID,
                UserName = n.Username,
                Name = n.Name,
                DisplayName = n.DisplayName,
                ImageUrl = n.ImageUrl
            }).SingleOrDefault();
            return View(model);
        }

        //
        // POST: /Category/Edit/5

        [HttpPost]
        public ActionResult Edit(DATA.Models.Category model)
        {
            string error = "";
            if (_repository.InsertOrUpdateCategory(model.ID, model.UserName, model.Name, model.DisplayName, model.ImageUrl, model.IsActive, out error))
                return RedirectToAction("Index");
            ModelState.AddModelError("", error);
            return View(model);
        }
    }
}
