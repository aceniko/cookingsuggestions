﻿using DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CookingSuggestions.Controllers
{
    [Authorize]
    public class MenuController : Controller
    {
        private readonly IData _repository;
        public MenuController() : this(new Data()) { }
        public MenuController(IData data) { _repository = data; }
        public ActionResult Index()
        {
            IList<GetMenuListResult> model = _repository.getMenuList();
            return View(model);
        }
        public ActionResult Create() {
            DATA.Models.Menu model = new DATA.Models.Menu();
            return View(model);
        }
        [HttpPost]
        public ActionResult Create(DATA.Models.Menu model) {
            string error = "";
            if (_repository.InsertOrUpdateMenu(null, model.Name, model.ImageUrl, model.IsActive, model.Sequence, out error))
                return RedirectToAction("Index");
            ModelState.AddModelError("", error);
            return View(model);
        }
        public ActionResult Edit(int id) {
            DATA.Models.Menu model = (from n in _repository.getMenuByID(id) select new DATA.Models.Menu { 
            ID=n.ID, Name = n.Name, ImageUrl = n.ImageUrl, IsActive = n.IsActive, Sequence = n.Sequence
            }).SingleOrDefault();
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(DATA.Models.Menu model) {
            string error = "";
            if (_repository.InsertOrUpdateMenu(model.ID, model.Name, model.ImageUrl, model.IsActive, model.Sequence, out error))
                return RedirectToAction("Index");
            ModelState.AddModelError("", error);
            return View(model);
        }
    }
}
