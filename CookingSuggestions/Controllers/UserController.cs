﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DATA;
using WebMatrix.WebData;
using System.Web.Security;
namespace CookingSuggestions.Controllers
{
    [Authorize(Roles="SuperAdmin")]
    public class UserController : Controller
    {
        private readonly IData _repository;

        public UserController() : this(new Data()) { }

        public UserController(IData data) {
            _repository = data;
        }

        public ActionResult Index()
        {
            IList<GetUserListResult> model = _repository.getUsers();
            return View(model);
        }
        public ActionResult Create() {
            DATA.Models.User model = new DATA.Models.User();

            return View(model);
        }
        [HttpPost]
        public ActionResult Create(DATA.Models.User model) {
            string error = string.Empty;
            try
            {
                WebSecurity.CreateUserAndAccount(model.Username, model.Password);
               // WebSecurity.Login(model.UserName, model.Password);
                Roles.AddUserToRole(model.Username, "Admin");
                return RedirectToAction("Index", "Home");
            }
            catch (MembershipCreateUserException e)
            {
                ModelState.AddModelError("","error");
            }
            return View(model);
            //if(_repository.insertOrUpdateUser(null, model.Username, model.FirstName, model.LastName, out error))
            //    return RedirectToAction("Index");
            //ModelState.AddModelError("", error);
            //return View(model);
        }

        public ActionResult Edit(int id) {
            DATA.Models.User model = (from n in _repository.getUserByID(id) select new DATA.Models.User { 
                ID = n.ID,
                Username = n.Username,
                FirstName = n.FirstName,
                LastName = n.LastName
            }).SingleOrDefault();

            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(DATA.Models.User model) {
            string error = string.Empty;
            if(_repository.insertOrUpdateUser(model.ID, model.Username, model.FirstName, model.LastName, out error))
                return RedirectToAction("Index");
            ModelState.AddModelError("", error);
            return View(model);
        }
    }
}
