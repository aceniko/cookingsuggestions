﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DATA;
namespace CookingSuggestions.Controllers
{
    [Authorize]
    public class VideoController : Controller
    {
        private readonly IData _repository;

        public VideoController() : this(new Data()) { }
        public VideoController(IData data) { this._repository = data; }

        public ActionResult Index()
        {
            IList<GetVideosListResult> model = _repository.getVideos();
            return View(model);
        }
        public ActionResult Create() {
            DATA.Models.Video model = new DATA.Models.Video();
            model.CreateDate = DateTime.UtcNow;
            model.UpdateDate = DateTime.UtcNow;
            return View(model);
        }
        [HttpPost]
        public ActionResult Create(DATA.Models.Video model) {
            string error = "";
            if (_repository.InsertOrUpdateVideo(null, model.Username, model.Title, model.Description, model.URL, model.IsActive, out error))
                return RedirectToAction("Index");
            ModelState.AddModelError("", error);
            return View(model);
        }
        public ActionResult Edit(int id) {
            DATA.Models.Video model = (from n in _repository.getVideoByID(id) select new DATA.Models.Video { 
                ID = n.ID, Username = n.Username, IsActive = n.IsActive, URL = n.URL, Description = n.Description, Title = n.Title, CreateDate = n.DateCreate, UpdateDate = n.DateUpdated
            }).SingleOrDefault();
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(DATA.Models.Video model) {
            string error = "";
            if (_repository.InsertOrUpdateVideo(model.ID, model.Username, model.Title, model.Description, model.URL, model.IsActive, out error))
                return RedirectToAction("Index");
            ModelState.AddModelError("", error);
            return View(model);
        }
    }
}
