﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DATA;
namespace CookingSuggestions.Controllers
{
    [Authorize(Roles="SuperAdmin")]
    public class RecipeController : Controller
    {
        private readonly IData _repository;

        public RecipeController() : this(new Data()) { }
        public RecipeController(IData data) {
            _repository = data;
        }
        
        public ActionResult Index(int categoryID)
        {
            IList<GetRecipeForCategoryResult> model = _repository.getRecipesForCategoryID(categoryID);
            ViewBag.categoryID = categoryID;
            return View(model);
        }
        public ActionResult Create(int categoryID) {
            DATA.Models.Recipe model = new DATA.Models.Recipe();

            model.categoryID = categoryID;
            model.Username = User.Identity.Name;

            return View(model);
        }
        [HttpPost]
        public ActionResult Create(DATA.Models.Recipe model) {
            string error = "";
            if (_repository.InsertOrUpdateRecipe(null, model.Username, model.Title, model.Content, model.IsActive, model.ImageUrl, model.HashID, model.Day, model.HolidayTag, out error))
                return RedirectToAction("Index", new { categoryID = model.categoryID });
            ModelState.AddModelError("", error);
            return View(model);
        }
        public ActionResult Edit(int id, int categoryID) {
            DATA.Models.Recipe model = (from n in _repository.getRecipeByID(id)
                                        select new DATA.Models.Recipe
                                        {
                                            ID=n.ID,
                                            Username = n.Username, 
                                            Title = n.Title,
                                            Content = n.Content,
                                            ImageUrl = n.ImageUrl,
                                            IsActive = n.IsActive,
                                            HashID = n.HashID,
                                            CreateDate = n.CreateDate,
                                            UpdateDate = n.UpdateDate,
                                            Day = n.Day,
                                            HolidayTag = n.HolidayTag
                                        }).SingleOrDefault();
            model.categoryID = categoryID;
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(DATA.Models.Recipe model) {
            string error = "";
            if (_repository.InsertOrUpdateRecipe(model.ID, model.Username, model.Title, model.Content, model.IsActive, model.ImageUrl, model.HashID, model.Day, model.HolidayTag, out error))
                return RedirectToAction("Index", new { categoryID = model.categoryID });
            ModelState.AddModelError("", error);
            return View(model);
        }
    }
}
