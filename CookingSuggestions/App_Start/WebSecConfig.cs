﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebMatrix.WebData;

namespace CookingSuggestions.App_Start
{
    public class WebSecConfig
    {
        public static void RegisterWebSec()
        {
            WebSecurity.InitializeDatabaseConnection("dbCookingSuggestions", "User", "ID", "Username", autoCreateTables: true);
        }
    }
}